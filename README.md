# Iosevka Nerd Font Custom

## Source

* Iosevka https://github.com/be5invis/Iosevka
* Nerd Font patcher https://github.com/ryanoasis/nerd-fonts
* Patch Iosevka Nerd Font https://github.com/awnion/custom-iosevka-nerd-font